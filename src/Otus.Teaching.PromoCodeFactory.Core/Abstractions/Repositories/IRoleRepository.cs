﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRoleRepository : IRepository<Role>
    {
        Task<List<Role>> GetRolesByNames(string[] rolesNames);
    }
}
