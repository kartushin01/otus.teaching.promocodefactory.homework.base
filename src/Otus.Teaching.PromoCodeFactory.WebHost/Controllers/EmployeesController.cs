﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRoleRepository _roleRepository;
        public EmployeesController(IRepository<Employee> employeeRepository, IRoleRepository roleRepository)
        {
            _employeeRepository = employeeRepository;
     
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(EmployeeRequest model)
        {

            Employee employeeToCreate = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                AppliedPromocodesCount = model.AppliedPromocodesCount,
                Roles = await _roleRepository.GetRolesByNames(model.RoleNames)
            };

            try
            {
                await _employeeRepository.CreateAsync(employeeToCreate);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }


            return StatusCode((int)HttpStatusCode.Created);
        }


        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployeeAsync(Guid id, EmployeeRequest model)
        {

            Employee fromDb = await _employeeRepository.GetByIdAsync(id);

            if (fromDb == null)
            {
                return BadRequest();
            }

            fromDb.FirstName = model.FirstName;
            fromDb.LastName = model.LastName;
            fromDb.AppliedPromocodesCount = model.AppliedPromocodesCount;
            fromDb.Email = model.Email;
            fromDb.Roles =  await _roleRepository.GetRolesByNames(model.RoleNames);

            try
            {
                await _employeeRepository.UpdateAsync(fromDb);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok("successful");
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {

            Employee toDelete = await _employeeRepository.GetByIdAsync(id);

            if (toDelete == null)
            {
                return NotFound();
            }
            try
            {
                await _employeeRepository.DeleteAsync(toDelete);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok("employee deleted");
        }

    }
}