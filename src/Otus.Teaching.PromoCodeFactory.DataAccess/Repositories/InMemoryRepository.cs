﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }
        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task CreateAsync(T entity)
        {
            var data = Data as List<T>; 
            data.Add(entity);
            Data = data;

            //List<T> temp = new List<T>();
            //temp.Add(entity);
            //var baseEntities = temp.Concat(data);
            //Data = baseEntities;
            
        }

        public async Task UpdateAsync(T entity)
        {
            var data = Data as List<T>;
            var fromDb = data.FirstOrDefault(x => x.Id == entity.Id);
            data.Remove(fromDb);
            data.Add(entity);
            Data = data;
        }

        public async Task DeleteAsync(T entity)
        {
            var data = Data as List<T>;
            var fromDb = data.FirstOrDefault(x => x.Id == entity.Id);
            data.Remove(fromDb);
            Data = data;
        }
    }
}