﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class RoleRepository : InMemoryRepository<Role> , IRoleRepository
    {
        private readonly IEnumerable<Role> Data;

        public RoleRepository(IEnumerable<Role> data) : base(data)
        {
            Data = data;
        }

        public async Task<List<Role>> GetRolesByNames(string[] rolesNames)
        {
            return Data.Where(x => rolesNames.Contains(x.Name)).ToList();
        }
    }
}
